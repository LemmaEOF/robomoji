# robomoji

robot,,,,,, fedi,,, emoj,,,,, beep,,,

## Licensing

Robomoji are modified [Mutant Standard emoji](https://mutant.tech),
which are licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/). Robomoji is also licenced under [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/).

see [Accreditation.xml](Accreditation.xml) for something appropriate to drop into a ActivityPub server to provide credit legally required by CC-BY-NC-SA.

Emojis here, other than `ms_robot.png`, are made by their own authors,
represented by the folders in this repository.

All emojis MUST be 256x256 to be accepted into this repo.

## TODO

- scripts to make them mastodon-compatible (less than 50kb, downscale to 100x100, etc)
- scripts to make pleroma emoji packs out of this
